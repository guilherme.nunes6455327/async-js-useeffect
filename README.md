# React async

App that simulates a file system.

## Challenge gist

https://gist.github.com/jprask/9f6870db5f851d08bcbff42f1e9a1f03

## Run the app

install packages with: `npm install`

run the app whit: `npm run dev`
