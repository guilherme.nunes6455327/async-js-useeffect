const API_URL = 'https://api.restful-api.dev/objects'

export function createNote(title, body) {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      data: {
        title,
        body
      }
    })
  }

  return fetch(API_URL, options)
    .then(function (response) {
      if (response.ok) {
        return response.json()
      } else {
        throw new Error('Error: ' + response.status)
      }
    })
    .then(response => ({ id: response.id, ...response.data }))
}

export function fetchNote(noteId) {
  return fetch(`${API_URL}/${noteId}`)
    .then(function (response) {
      if (response.ok) {
        return response.json()
      } else {
        throw new Error('Error: ' + response.status)
      }
    })
    .then(response => response.data)
}
