import { useState } from "react"
import './note-form.css'

export function NoteForm({ onSubmit }) {
  const [title, setTitle] = useState('')
  const [body, setBody] = useState('')

  const handleSubmit = (event) => {
    event.preventDefault()
    onSubmit({ title, body })
    setTitle('')
    setBody('')
  }

  return (
    <form className="note-form" onSubmit={handleSubmit}>
      <label htmlFor="title">Title:</label>
      <input
        type="text"
        id="title"
        name="title"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        required
      />

      <label htmlFor="body">Body:</label>
      <textarea
        id="body"
        name="body"
        value={body}
        onChange={(e) => setBody(e.target.value)}
        rows="4"
        required
      >
      </textarea>

      <button type="submit">add note</button>
    </form>
  )
}
