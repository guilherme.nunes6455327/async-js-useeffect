
export function NoteList({ notes, handleNoteClick }) {
  return (
    <div>
      <h2>existing notes:</h2>
      <ul id="notes">
        {notes.map((note) => (
          <li key={note.id} onClick={() => handleNoteClick(note.id)}>
            {note.title}
          </li>
        ))}
      </ul>
    </div>
  )
}
