import { useState } from 'react'
import * as API from './api'
import { NoteForm } from './NoteForm'
import { NoteList } from './NoteList'
import { NoteDetails } from './NoteDetails'
import './notes.css'

export function Notes() {
  const [notes, setNotes] = useState([])
  const [selectedNote, setSelectedNote] = useState(null)

  const onSubmitForm = ({ title, body }) => {
    API.createNote(title, body)
      .then((note) => {
        setNotes([...notes, note])
      })
      .catch((error) => {
        console.error('Error creating note:', error)
      })
  }

  const handleNoteClick = (noteId) => {
    API.fetchNote(noteId)
      .then((note) => {
        setSelectedNote(current => current ?? note)
      })
      .catch((error) => {
        console.error('Error fetching note:', error)
      })
  }

  const handleDialogClose = () => {
    setSelectedNote(null)
  }

  return (
    <section className='notes-container'>
      <header>
        <h1>notes</h1>
      </header>

      <NoteForm onSubmit={onSubmitForm} />
      <NoteList notes={notes} handleNoteClick={handleNoteClick} />
      <NoteDetails note={selectedNote} handleClose={handleDialogClose} />
    </section>
  )
}
