import { useEffect, useRef } from "react"

export function NoteDetails({ note, handleClose }) {
  const dialogRef = useRef(null)

  useEffect(() => {
    if (!note) return

    dialogRef.current.showModal()
  }, [note])

  if (!note) return null

  return (
    <dialog ref={dialogRef} onClick={handleClose}>
      <h2>{note.title}</h2>
      <p>{note.body}</p>
      <button type="button" onClick={handleClose}>
        Close
      </button>
    </dialog>
  )
}
