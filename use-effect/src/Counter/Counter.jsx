import { useEffect, useState } from "react"
import './counter.css'

export function Counter() {
  const [targetCount, setTargetCount] = useState(30)
  const [count, setCount] = useState(30)
  const [isRunning, setIsRunning] = useState(true)

  const handleClick = () => {
    setCount(targetCount)
    setIsRunning(true)
  }

  const computeCounterText = () => {
    if (!isRunning) return ''

    return count === 0 ? 'done!' : count
  }

  useEffect(() => {
    if (count === 0) return setIsRunning(false``)

    // setInterval vs setTimeout
    const timeoutId = setTimeout(() => setCount(prev => prev - 1), 1000)

    return () => clearTimeout(timeoutId)
  }, [isRunning, setIsRunning, count, setCount])

  return (
    <section className="counter-container">
      <h1>counter</h1>

      <label>
        count down to:
        <input type="number" min="1" value={targetCount} onChange={(e) => setTargetCount(e.target.value)} />
      </label>

      <button onClick={handleClick}>
        start!
      </button>

      <h2>{ computeCounterText() }</h2>
    </section>
  )
}
