import { Counter } from "./Counter";
import { Notes } from "./Notes";

export function App() {
  return (
    <>
      <Counter />
      <hr style={{ width: '100%', margin: '1em' }} />
      <Notes />
    </>
  )
}
