# Filesystem

The behavior of the app is described in the gist https://gist.github.com/jprask/9f6870db5f851d08bcbff42f1e9a1f03.

[nvm](https://github.com/nvm-sh/nvm) is recommended to work with this app. after navigating to the project root folder, run `nvm use` and make sure node.js is the correct version defined in `.nvmrc`

There's a few useful commands to run when developing.

`npm run dev` - start the dev server

`npm run test` - run unit tests

# API

To run this app, you must also run the API on port 3000 in your local machine. To do this, you can run the following command

```bash
cd ../fs-api
npm run dev
```

further documentation for the API in found on [fs-api/README.md](../fs-api/README.md)
