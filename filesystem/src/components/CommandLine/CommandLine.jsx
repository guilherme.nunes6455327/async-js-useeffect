import { useState } from 'react'
import {
  moveTo,
  createFolder,
  createFile,
  remove,
  move,
  concat,
  find,
} from '../../services/filesystem'
import './CommandLine.css'
import { LOAD_STATES } from '../../utils/loadStates'

export function CommandLine({
  rootId,
  setSelectedFolderId,
  selectedFolderId,
  setLoadState,
}) {
  const [input, setInput] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState(null)
  const [messages, setMessages] = useState(null)

  const handleUpdateState = () => {
    setLoadState(LOAD_STATES.PENDING)
  }

  const handleUpdateMessage = (message) => {
    setMessages(message)
  }

  const handleSubmit = (event) => {
    const tokens = input.match(/(?:[^\s"]+|"[^"]*")+/g)

    const [command, ...rawArgs] = tokens

    const args = rawArgs.map((arg) => arg.replace(/^"(.*)"$/, '$1'))

    const commands = {
      cd() {
        const [rawNamePath] = args
        const path = rawNamePath.split('/').filter(Boolean)

        setIsLoading(true)

        moveTo({ rootId, path })
          .then((newSelectedFolder) => {
            setSelectedFolderId(newSelectedFolder.id)
          })
          .catch(() => setError('invalid folder'))
          .finally(() => setIsLoading(false))
      },

      mkdir() {
        const [dirName] = args

        if (!dirName) {
          setError('Missing folder name')
          return
        }

        setIsLoading(true)

        createFolder(dirName, selectedFolderId)
          .then(() => {
            handleUpdateState()
          })
          .catch((error) => {
            if (error.message === 'NAME_NOT_UNIQUE') {
              setError('Folder name is not unique')
            } else {
              setError('An error occurred while creating the folder')
            }
          })
          .finally(() => setIsLoading(false))
      },

      touch() {
        const [fileName, fileContent] = args

        if (!fileName || !fileContent) {
          setError('Usage: touch <file_name> <file_contents>')
          return
        }

        setIsLoading(true)

        createFile(selectedFolderId, fileName, fileContent)
          .then(() => {
            handleUpdateState()
          })
          .catch((error) => {
            if (error.message === 'NAME_NOT_UNIQUE') {
              setError('File name is not unique')
            } else {
              setError('An error occurred while creating the file')
            }
          })
          .finally(() => setIsLoading(false))
      },

      rm() {
        const [objectName] = args

        if (!objectName) {
          setError('Usage: rm <object_name>')
          return
        }

        setIsLoading(true)

        remove(objectName, selectedFolderId)
          .then(() => {
            handleUpdateState()
          })
          .catch((error) => {
            if (error.message === 'FOLDER_NOT_EMPTY') {
              setError('Folder is not empty')
            } else if (error.message === 'OBJECT_NOT_FOUND') {
              setError('Folder or file not found')
            } else {
              setError('An error occurred while deleting the file')
            }
          })
          .finally(() => setIsLoading(false))
      },

      mv() {
        const [objectName, newFolderPath] = args

        if (!objectName || !newFolderPath) {
          setError('Usage: mv <object_name> <new_folder_path>')
          return
        }

        setIsLoading(true)

        const path = newFolderPath.split('/').filter(Boolean)
        moveTo({ rootId, path })
          .then((newFolder) => {
            const newFolderId = newFolder.id

            move(selectedFolderId, newFolderId, objectName)
              .then(() => {
                handleUpdateState()
              })
              .catch((error) => {
                if (error.message === 'INVALID_OPERATION') {
                  setError('Invalid operation')
                } else if (error.message === 'OBJECT_NOT_FOUND') {
                  setError('Folder or file not found')
                } else if (error.message === 'NAME_NOT_UNIQUE') {
                  setError('Object name is not unique in destination folder')
                } else {
                  setError('An error occurred while deleting the file')
                }
              })
          })
          .catch(() => setError('invalid folder'))
          .finally(() => setIsLoading(false))
      },

      concat() {
        setIsLoading(true)

        concat(selectedFolderId)
          .then((concatText) => {
            handleUpdateMessage(concatText)
          })
          .catch((error) => {
            if (error.message === 'INVALID_OPERATION') {
              setError('No files in the current folder')
            } else {
              setError('An error occurred while concat contents')
            }
          })
          .finally(() => {
            setIsLoading(false)
          })
      },

      find() {
        const [searchText] = args

        if (!searchText) {
          setError('Usage: find <target>')
          return
        }

        setIsLoading(true)

        find(selectedFolderId, searchText)
          .then((searchResults) => {
            handleUpdateMessage(searchResults)
          })
          .catch((error) => {
            if (error.message === 'INVALID_OPERATION') {
              setError('No files in the current folder')
            } else if (error.message === 'NO_MATCHES_FOUND') {
              setError('No matches found')
            } else {
              setError('An error occurred while concat contents')
            }
          })
          .finally(() => {
            setIsLoading(false)
          })
      },
    }

    const operation = commands[command]

    event.preventDefault()
    setError(null)
    setMessages(null)
    setInput('')

    if (!operation) return setError(`invalid input: ${input}`)

    operation()
  }

  return (
    <div className="command-line-container">
      {error && <p className="command-line-error">{error}</p>}
      {messages &&
        messages.map((message, i) => {
          return (
            <p key={i} className="command-line-message">
              {message}
            </p>
          )
        })}

      <form onSubmit={handleSubmit}>
        <input
          type="text"
          id="title"
          name="title"
          value={input}
          onChange={(e) => setInput(e.target.value)}
          required
          disabled={isLoading}
        />
      </form>
    </div>
  )
}
