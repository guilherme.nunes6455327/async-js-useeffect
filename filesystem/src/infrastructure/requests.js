const BASE_URL = 'http://localhost:3000/api'

function handleFetch(res) {
  if (res.ok) {
    return res.json()
  } else {
    throw new Error('Error: ' + res.status)
  }
}

export function fetchAll() {
  return fetch(`${BASE_URL}/objects`).then(handleFetch)
}

export function fetchObject(id) {
  return fetch(`${BASE_URL}/object/${id}`).then(handleFetch)
}

export function put(id, data) {
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  }

  return fetch(`${BASE_URL}/object/${id}`, options).then(handleFetch)
}

export function deleteObject(id) {
  const options = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
  }

  return fetch(`${BASE_URL}/object/${id}`, options).then(handleFetch)
}

export function post(data) {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  }

  return fetch(`${BASE_URL}/objects`, options).then(handleFetch)
}
