import { chain } from '../utils'
import {
  fetchObject,
  put,
  post,
  deleteObject,
} from '../infrastructure/requests'

export function moveTo({ rootId, path }) {
  const fetchRoot = () => fetchObject(rootId)
  const moves = path.map((step) => (parent) => {
    return Promise.all(parent.children.map(fetchObject)).then((children) =>
      children.find((child) => child.name === step),
    )
  })

  return chain(fetchRoot, ...moves)
}

function updateFolderChildren(childrenId, parentId, operation) {
  let updatedChildren

  return fetchObject(parentId)
    .then((parentData) => {
      if (operation === 'add') {
        updatedChildren = [...parentData.children, childrenId]
      } else if (operation === 'remove') {
        updatedChildren = parentData.children.filter(
          (child) => child !== childrenId,
        )
      } else {
        throw new Error('Invalid operation')
      }

      const updatedData = {
        ...parentData,
        children: updatedChildren,
      }

      return put(parentId, updatedData)
    })
    .catch((error) => {
      console.error('Error:', error.message)
    })
}

function verifyUniqueName(name, parentId) {
  return fetchObject(parentId).then((parent) => {
    return Promise.all(
      parent.children.map((childrenList) => {
        return fetchObject(childrenList).then((response) => {
          if (response.name === name) {
            throw new Error('NAME_NOT_UNIQUE')
          }
        })
      }),
    )
  })
}

export function createFolder(folderName, parentId) {
  const data = {
    id: folderName,
    type: 'folder',
    name: folderName,
    children: [],
  }

  if (parentId) {
    return verifyUniqueName(folderName, parentId)
      .then(() => {
        return post(data)
      })
      .then((newFolder) => {
        return updateFolderChildren(newFolder.id, parentId, 'add')
      })
      .catch((error) => {
        console.error('An error occurred:', error.message)
        throw error
      })
  }

  return post(data).catch((error) => {
    console.error('An error occurred:', error.message)
  })
}

export function createFile(folderId, name, content) {
  const data = {
    id: name,
    type: 'file',
    name: name,
    contents: content,
  }

  return verifyUniqueName(name, folderId)
    .then(() => {
      return post(data)
    })
    .then((newFile) => {
      return updateFolderChildren(newFile.id, folderId, 'add')
    })
    .catch((error) => {
      console.error('An error occurred:', error.message)
      throw error
    })
}

export function remove(objectName, parentId) {
  return findByName(objectName, parentId).then((objectId) => {
    return fetchObject(objectId)
      .then((object) => {
        if (object.children?.length > 0) throw new Error('FOLDER_NOT_EMPTY')
      })
      .then(() => {
        return updateFolderChildren(objectId, parentId, 'remove')
      })
      .then(() => {
        return deleteObject(objectId)
      })
      .catch((error) => {
        console.error('An error occurred:', error.message)
        throw error
      })
  })
}

export function move(currentFolderId, newFolderId, objectName) {
  return findByName(objectName, currentFolderId)
    .then((objectId) => {
      return Promise.all([
        fetchObject(currentFolderId),
        fetchObject(newFolderId),
        fetchObject(objectId),
      ]).then(([currentFolder, newFolder, object]) => {
        if (currentFolder.type !== 'folder' || newFolder.type !== 'folder') {
          throw new Error('INVALID_OPERATION')
        }

        if (!currentFolder.children.includes(objectId)) {
          throw new Error('INVALID_OPERATION')
        }

        return verifyUniqueName(object.name, newFolderId).then(() => {
          return Promise.all([
            updateFolderChildren(objectId, newFolderId, 'add'),
            updateFolderChildren(objectId, currentFolderId, 'remove'),
          ]).catch((error) => {
            console.error('An error occurred:', error.message)
          })
        })
      })
    })
    .catch((error) => {
      console.error('An error occurred:', error.message)
      throw error
    })
}

function folderChildrenParse(folderId) {
  return fetchObject(folderId).then((folder) => {
    if (folder.type !== 'folder') {
      throw new Error('INVALID_OPERATION')
    }

    return Promise.all(
      folder.children.map((childId) => {
        return fetchObject(childId)
      }),
    )
  })
}

function selectChildrenFiles(folderId) {
  return folderChildrenParse(folderId).then((childrenArr) => {
    const fileArr = childrenArr.filter((child) => child.type === 'file')

    if (fileArr.length === 0) {
      throw new Error('INVALID_OPERATION')
    }

    return fileArr
  })
}

function findByName(objectName, parentId) {
  return folderChildrenParse(parentId)
    .then((childrenArr) => {
      const filteredChildren = childrenArr.filter(
        (child) => child.name === objectName,
      )

      if (filteredChildren.length === 0) throw new Error('OBJECT_NOT_FOUND')

      return filteredChildren[0].id
    })
    .catch((error) => {
      console.error('An error occurred:', error.message)
      throw error
    })
}

export function concat(folderId) {
  return selectChildrenFiles(folderId)
    .then((fileArr) => {
      return fileArr.map((file) => file.contents)
    })
    .catch((error) => {
      console.error('An error occurred:', error.message)
      throw error
    })
}

export function find(folderId, searchText) {
  return selectChildrenFiles(folderId)
    .then((fileArr) => {
      const matchingFiles = fileArr.filter((file) => {
        return file.contents.includes(searchText)
      })

      if (matchingFiles.length === 0) throw new Error('NO_MATCHES_FOUND')

      return matchingFiles.map((file) => file.name)
    })
    .catch((error) => {
      console.error('An error occurred:', error.message)
      throw error
    })
}
